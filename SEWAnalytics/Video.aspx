﻿<%@ Page Language="C#" Async="true" AutoEventWireup="true" CodeBehind="Video.aspx.cs" Inherits="SEWAnalytics.Video" %>

<!DOCTYPE html>

<html lang="en">

<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta charset="utf-8" />
    <title>South East Water</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!-- STYLESHEETS -->
    <!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
    <link rel="stylesheet" type="text/css" href="css/reset.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/dashboard.css" />
   
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>



    
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <!-- DATE RANGE PICKER -->

    <!-- FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css' />
</head>
<script>

    function changeContent(val) {

        if (val == 0) {

            document.getElementById("content1").style.display = "none";
            document.getElementById("content2").style.display = "block"

        } else {

            document.getElementById("content1").style.display = "block";
            document.getElementById("content2").style.display = "none"

        }
    }

    $(document).ready(function () {
        $("#fileUpload").on('change', function () {
            //get the file name
            var fileName = $(this).val().replace('C:\\fakepath\\', " ");
            //replace the "Choose a file" label
            $(this).next('.custom-file-label').html(fileName);
        });
    });


</script>


<body class="login body_bg">
    <!-- PAGE -->

    <header class="dashboard_header">
        <div class="col-md-12">
            <div class="col-md-6">

                <image src="images/SEWlogo.png" class="logo_dashboard"></image>

            </div>

            <div class="col-md-6">

              <div class="logoutdiv">
                  <a href="/">
                      <span class="logout"> Logout </span>
                  </a>
              </div>

            </div>

        </div>
    </header>

    <div class="container-fluid">
        <div class="col-md-12 mainBody" id="content1" style="display: block">
            <div class="col-md-6 leftBody">
                <div class="centerbody col-md-12">
                    <form runat="server">
                        <div class="custom-file">
                            <%--<label id="lblUploadFile" class="custom-file-label" runat="server"  for="fileUpload"></label>--%>
                          
                            <input type="file" runat="server" accept=".mpeg,.mpg,.mp4"  class="custom-file-input" id="fileUpload" name="filename">
                            <label id="lblUploadFile" class="custom-file-label" runat="server"  for="fileUpload">Choose file</label>
                        </div>
                        
                        <div class="buttonclass">
                           <%-- <asp:Button ID="btnProcessVideo" class="btn btn-primary" runat="server" Text="Process Video" OnClick="btnProcessVideo_Click"/>--%>
                            <button type="submit" ID="btnProcessVideo1" runat="server" onserverclick="btnProcessVideo_Click" class="btn btn-primary">Upload Video</button>
                            <%-- <button type="submit" class="btn btn-primary">Upload Video</button>--%>
                        </div>

                         <div class="buttonclass">
                           <%-- <asp:Button ID="btnProcessVideo" class="btn btn-primary" runat="server" Text="Process Video" OnClick="btnProcessVideo_Click"/>--%>
                            <button type="submit" ID="btnImageAnalytics" runat="server" onserverclick="btnImageAnalytics_Click" class="btn btn-primary">Process</button>
                            <%-- <button type="submit" class="btn btn-primary">Upload Video</button>--%>
                        </div>
                        <%--<div class="buttonclass">
                              <button type="submit" ID="btnImageAnalytics1" runat="server" disabled Text="Start Image Analysis" onserverclick="btnImageAnalytics_Click" class="btn btn-primary">Upload Video</button>
                        </div>--%>
                    </form>
                </div>
            </div>
            <div class="col-md-6 rightBody">
                <div class="centerbodyright">
                    <div class="reportimg">
                        <a href="#" onclick="changeContent(0)">

                            <image src="images/powerbi.png" class="powerbiimage" />

                            <div class="text">
                                    <span>Check Report</spna>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 mainBody" id="content2" style="display: none">

            <div class="col-md-12 bodyheader">

                <button onclick="changeContent(1)" class="btnclose"> Close Report </button>

            </div>

            <div class="col-md-12">

                    <iframe width="1200" height="600" src="https://app.powerbi.com/view?r=eyJrIjoiNTY5M2JjOTItNmNmZC00NDc4LWFhZjMtZTgyZDI5NWRjOTJkIiwidCI6IjI1OGFjNGU0LTE0NmEtNDExZS05ZGM4LTc5YTllMTJmZDZkYSIsImMiOjEwfQ%3D%3D" frameborder="0" allowFullScreen="true"></iframe>

            </div>

        </div>

    </div>
</body>

</html>