﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace SEWAnalytics
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {

           

            string CachedfilePath = Server.MapPath("~/Files/");
            System.IO.DirectoryInfo di = new DirectoryInfo(CachedfilePath);

            try
            {
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}