﻿using System;
using System.IO;
using System.Configuration;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Drawing.Imaging;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Emgu.CV.CvEnum;
using System.Threading;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using Microsoft.WindowsAzure.Storage.Auth;
using System.Globalization;
using System.Net.Http.Headers;
using System.Diagnostics;
using Microsoft.Azure.ServiceBus;
using System.Text;
//using System.Net.Http.Formatting;
//using System.Net.Http.Headers;

namespace SEWAnalytics
{
    public partial class Video : System.Web.UI.Page
    {
        private string filePath = string.Empty;
        private string ImageContainerName = "master";
        private string ImagesZipContainerName = "imageszip";
        private string storageConnection = string.Empty;
        private CloudStorageAccount cloudStorageAccount = null;
        private CloudBlobClient cloudBlobClient = null;
        private QueueClient queueClient;

        protected void Page_Load(object sender, EventArgs e)
        {
            storageConnection = ConfigurationManager.AppSettings["ConnectionString"];
            cloudStorageAccount = CloudStorageAccount.Parse(storageConnection);
            cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();
            btnProcessVideo1.Disabled = false;
            btnImageAnalytics.Disabled = true;
            //DeleteLocalFiles();
            //lnkPowerBI.NavigateUrl = ConfigurationManager.AppSettings["PowerBILink"];
        }

        private void DeleteLocalFiles()
        {

            string CachedfilePath = Page.Server.MapPath("~/Files/");
            System.IO.DirectoryInfo di = new DirectoryInfo(CachedfilePath);

            try
            {
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void fileUpload_Submit(object sender, EventArgs e)
        {
        }
        protected async void btnProcessVideo_Click(object sender, EventArgs e)
        {

            string fileUploadPath = fileUpload.PostedFile.FileName;
            int fileSize = fileUpload.PostedFile.ContentLength;
            //lblerror.Text = string.Empty;
            if (fileUploadPath == string.Empty)
            {
                //lblerror.Text = "Enter the file Path";
                return;
            }

            if (fileSize > 524288000)
            {
                //lblerror.Text = "Video size must be less than or equal to 500 MB";
                return;
            }

            btnProcessVideo1.Disabled = true;
            string fileName = Path.GetFileNameWithoutExtension(fileUploadPath).ToLower();
            string folder = Server.MapPath("~/Files/");
            string folderPath = (Path.Combine(folder, fileName));

            //Image1.Visible = true;

            Directory.CreateDirectory(folderPath);
            filePath = Path.Combine(folderPath, fileName);
            fileUpload.PostedFile.SaveAs(filePath);

            try
            {
                //int intervalValue = Convert.ToInt16(drpInterval.SelectedValue);
                //int intervalValue = 1;
                //CreateFrames(filePath, framespath, 1);
                var progressIndicator = new Progress<int>(ReportProgress);
                int index = await EmguCreateFrame(filePath, progressIndicator);
            }
            catch (Exception ex)
            {

            }
            //DeleteFile(filePath);
        }

        private void ReportProgress(int value)
        {
            //mage1.Visible = true;
            int max = 100;
           // lblProgress1.Text = value.ToString();
            string percent = Convert.ToDouble(value * 100 / max).ToString("0");
            string str = percent + "% complete (" + value.ToString() + " of " + max.ToString() + ")";
            //btnLiteral.Text = str + "<TABLE cellspacing=0 cellpadding=0 border=1 width=200><TR><TD bgcolor=#000066 width=" + percent.ToString() + "%> </TD><TD bgcolor=#FFF7CE> </TD></TR></TABLE>";


            if (value==100)
            {
                
                try
                {
                    //btnImageAnalytics1.Disabled = false;
                    //Image1.Visible = false;

                    btnImageAnalytics.Disabled = false;

                    //Image1.Visible = false;
                    //string fileName = fileUpload.FileName;
                    //string fileName1 = Path.GetFileNameWithoutExtension(fileName).ToLower();
                    //string folder = Server.MapPath("~/Files/");
                    //string folderPath = (Path.Combine(folder, fileName1));

                    //filePath = Path.Combine(folderPath, fileName);
                    //FileInfo file = new FileInfo(filePath);
                    ////while (IsFileLocked(file))
                    ////    Thread.Sleep(1000);
                    //file.Delete();
                }
                catch(Exception ex)
                {
                }
            }
           
            //Update the UI to reflect the progress value that is passed back.
        }

        private void CreateZipFromFrames()
        {
            //create a container 
            CloudBlobContainer cloudBlobContainer = CreateOrGetContainer(ImageContainerName);
            var list = cloudBlobContainer.ListBlobs();
            var blobs = list.OfType<CloudBlockBlob>();
            List<string> blobNames = list.OfType<CloudBlockBlob>().Select(b => b.Name).ToList();

            string folder = Page.Server.MapPath("~/ZipFiles/");
            string filePath = (Path.Combine(folder, "validation.zip"));

            FileInfo zipFile = CreateZip(filePath, blobNames);
            UploadFileToStorage(zipFile, filePath);
            //File.Delete(filePath);
        }

        private void UploadFileToStorage(FileInfo fileInfo, string FilePath)
        {
            string fileName = Path.GetFileName(FilePath).ToLower();  
            //create a container 
            CloudBlobContainer cloudBlobContainer = CreateOrGetContainer(ImagesZipContainerName);

            // Retrieve reference to a blob named "myblob".
            CloudBlockBlob blockBlob = cloudBlobContainer.GetBlockBlobReference(fileName);

            //blockBlob.Properties.
            // Upload the zip and store it in the blob 
            using (FileStream fs = fileInfo.OpenRead())
                blockBlob.UploadFromStream(fs);
        }

        private FileInfo CreateZip(string zipFileName, List<string> filesToZip)
        {     
            //create a container 
            CloudBlobContainer cloudBlobContainer = CreateOrGetContainer(ImageContainerName);

            FileInfo zipFile = new FileInfo(zipFileName);
            FileStream fs = zipFile.Create();
            DateTime dt1 = DateTime.Now;
            using (ZipArchive zip = new ZipArchive(fs, ZipArchiveMode.Create))
            {
                foreach (string fileName in filesToZip)
                {
                    var blob = cloudBlobContainer.GetBlockBlobReference(fileName);
                    string folder = Page.Server.MapPath("~/ZipFiles/");
                    string folderPath = (Path.Combine(folder, fileName));

                    blob.DownloadToFile(folderPath, FileMode.Create);
                    zip.CreateEntryFromFile(folderPath, Path.GetFileName(folderPath), CompressionLevel.Optimal);

                    File.Delete(folderPath);
                }
            }
            DateTime dt2 = DateTime.Now;
            TimeSpan sp = dt2 - dt1;

            return zipFile;
        }

        private async Task<int> EmguCreateFrame(string videoFileName, IProgress<int> progress)
        {
            CloudBlobContainer cloudBlobContainer = CreateOrGetContainer(ImageContainerName);
            Mat frame;
            Bitmap image1 = null;
            CloudBlockBlob blockBlob = null;
            string fileName = string.Empty;

            string fileNameInitial = Path.GetFileNameWithoutExtension(videoFileName).ToLower();
            Boolean reading = true;
            int i = 0;

            using (VideoCapture capture = new VideoCapture(videoFileName))
            {
                double framecount = capture.GetCaptureProperty(CapProp.FrameCount);
                double framenumber = 0;

                double FramePersecond = capture.GetCaptureProperty(CapProp.Fps);
                int processCount = await Task.Run<int>(async()=>
                {
                   
                    while (reading)
                    {
                        frame = capture.QueryFrame();
                        framenumber = capture.GetCaptureProperty(CapProp.PosFrames);
                        if (frame != null)
                        {
                            if (i % 100 == 0)
                            {
                                //double timestamp = capture.GetCaptureProperty(CapProp.PosMsec) * 1000;
                                int timestamp = Convert.ToInt32(framenumber / FramePersecond);
                                image1 = frame.ToImage<Bgr, byte>().ToBitmap();
                                fileName = fileNameInitial + "_img_" + timestamp.ToString() + ".jpeg";

                                using (MemoryStream memoryStream = new MemoryStream())
                                {
                                    image1.Save(memoryStream, ImageFormat.Jpeg);
                                    memoryStream.Seek(0, SeekOrigin.Begin);
                                    blockBlob = cloudBlobContainer.GetBlockBlobReference(fileName);
                                    await blockBlob.UploadFromStreamAsync(memoryStream);
                                }
                                if (progress != null)
                                {
                                    progress.Report(Convert.ToInt16((framenumber * 100 / framecount)));
                                }
                            }
                        }
                        else
                        {
                            reading = false;
                        }
                        i++;
                    }
                    return Convert.ToInt32(framenumber);
                });
                return processCount;
            }
        }

        private CloudBlobContainer CreateOrGetContainer(string fileName)
        {
            //create a container 
            CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference(fileName);
            if (cloudBlobContainer.CreateIfNotExists())
            {
                cloudBlobContainer.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });
            }

            return cloudBlobContainer;
        }

        protected async void btnImageAnalytics_Click(object sender, EventArgs e)
        {
            btnImageAnalytics.Disabled = true;
            btnProcessVideo1.Disabled = false;
            CreateZipFromFrames();
            //isMLProcessing = true;
            //ListenToServiceBusQueue();
            await InvokeBatchExecutionService();
           
            //while (isMLProcessing)
            //{
            //    Thread.Sleep(4000);
            //}
        }

        private bool isMLProcessing = false;
        private void ListenToServiceBusQueue()
        {
            string sbConnectionString = "Endpoint=sb://sewservicebus.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=G4ON4+vtM3wo+CD5vco5qpeEoxEw/wXLhzZ2Ma4/oRA=";
            string sbQueueName = "sewqueue";

            queueClient = new QueueClient(sbConnectionString, sbQueueName);

            var messageHandlerOptions = new MessageHandlerOptions(ExceptionReceivedHandler)
            {
                MaxConcurrentCalls = 1,
                AutoComplete = false
            };
            queueClient.RegisterMessageHandler(ReceiveMessagesAsync, messageHandlerOptions);
        }
        private async Task ReceiveMessagesAsync(Message message, CancellationToken token)
        {
            //Console.WriteLine($"Received message: {Encoding.UTF8.GetString(message.Body)}");
            isMLProcessing = false;
            await queueClient.CompleteAsync(message.SystemProperties.LockToken);
        }

        private Task ExceptionReceivedHandler(ExceptionReceivedEventArgs exceptionReceivedEventArgs)
        {
            //Console.WriteLine(exceptionReceivedEventArgs.Exception);
            return Task.CompletedTask;
        }

        private async Task WriteFailedResponse(HttpResponseMessage response)
        {
            //Console.WriteLine(string.Format("The request failed with status code: {0}", response.StatusCode));

            // Print the headers - they include the requert ID and the timestamp, which are useful for debugging the failure
            //Console.WriteLine(response.Headers.ToString());

            string responseContent = await response.Content.ReadAsStringAsync();
            //Console.WriteLine(responseContent);
        }


        private void SaveBlobToFile(AzureBlobDataReference blobLocation, string resultsLabel)
        {
            const string OutputFileLocation = "myresults.csv"; // Replace this with the location you would like to use for your output file

            var credentials = new StorageCredentials(blobLocation.SasBlobToken);
            var blobUrl = new Uri(new Uri(blobLocation.BaseLocation), blobLocation.RelativeLocation);
            var cloudBlob = new CloudBlockBlob(blobUrl, credentials);

            //Console.WriteLine(string.Format("Reading the result from {0}", blobUrl.ToString()));
            cloudBlob.DownloadToFile(OutputFileLocation, FileMode.Create);

            //Console.WriteLine(string.Format("{0} have been written to the file {1}", resultsLabel, OutputFileLocation));
        }



        private void UploadFileToBlob(string inputFileLocation, string inputBlobName, string storageContainerName, string storageConnectionString)
        {
            // Make sure the file exists
            if (!File.Exists(inputFileLocation))
            {
                throw new FileNotFoundException(
                    string.Format(
                        CultureInfo.InvariantCulture,
                        "File {0} doesn't exist on local computer.",
                        inputFileLocation));
            }

            //Console.WriteLine("Uploading the input to blob storage...");

            var blobClient = CloudStorageAccount.Parse(storageConnectionString).CreateCloudBlobClient();
            var container = blobClient.GetContainerReference(storageContainerName);
            container.CreateIfNotExists();
            var blob = container.GetBlockBlobReference(inputBlobName);
            blob.UploadFromFile(inputFileLocation);
        }



        private void ProcessResults(BatchScoreStatus status)
        {

            bool first = true;
            foreach (var output in status.Results)
            {
                var blobLocation = output.Value;
                //Console.WriteLine(string.Format("The result '{0}' is available at the following Azure Storage location:", output.Key));
                //Console.WriteLine(string.Format("BaseLocation: {0}", blobLocation.BaseLocation));
                //Console.WriteLine(string.Format("RelativeLocation: {0}", blobLocation.RelativeLocation));
                //Console.WriteLine(string.Format("SasBlobToken: {0}", blobLocation.SasBlobToken));
                //Console.WriteLine();


                // Save the first output to disk
                if (first)
                {
                    first = false;
                    SaveBlobToFile(blobLocation, string.Format("The results for {0}", output.Key));
                }
            }
        }

        private async Task InvokeBatchExecutionService()
        {
            // How this works:
            //
            // 1. Assume the input is present in a local file (if the web service accepts input)
            // 2. Upload the file to an Azure blob - you'd need an Azure storage account
            // 3. Call the Batch Execution Service to process the data in the blob. Any output is written to Azure blobs.
            // 4. Download the output blob, if any, to local file

            const string BaseUrl = "https://uswestcentral.services.azureml.net/workspaces/ebab2a8c0f274814b2f2829e022d3eff/services/73ad1bf0d674462a83e007b559087702/jobs";

            //const string StorageAccountName = "mystorageacct"; // Replace this with your Azure Storage Account name
            //const string StorageAccountKey = "Dx9WbMIThAvXRQWap/aLnxT9LV5txxw=="; // Replace this with your Azure Storage Key
            //const string StorageContainerName = "mycontainer"; // Replace this with your Azure Storage Container name
            //string storageConnectionString = string.Format("DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1}", StorageAccountName, StorageAccountKey);

            const string StorageContainerName = "mlstudiotest"; // Replace this with your Azure Storage Container name
            //string storageConnectionString = string.Format("DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1}", StorageAccountName, StorageAccountKey);
            string storageConnectionString = storageConnection;
            const string apiKey = "jrBvTtYZlZf7A9myq2Wuoyzfiid/ATgeLaVoRAPUK3Nu5ADDEyXgGcy898iwMn4h7ZlKYLfZffMHQZFGWaIv/A=="; // Replace this with the API key for the web service
           
            // set a time out for polling status
            const int TimeOutInMilliseconds = 1200 * 1000; // Set a timeout of 20 minutes
            string csvfilePath = Page.Server.MapPath("~/CSV/") + "Studio.csv";

            UploadFileToBlob(csvfilePath /*Replace this with the location of your input file*/,
               "input1datablob.csv" /*Replace this with the name you would like to use for your Azure blob; this needs to have the same extension as the input file */,
               StorageContainerName, storageConnectionString);


            using (HttpClient client = new HttpClient())
            {
                var request = new BatchExecutionRequest()
                {

                    Inputs = new Dictionary<string, AzureBlobDataReference>()
                    {

                        {
                            "input1",
                            new AzureBlobDataReference()
                            {
                                ConnectionString = storageConnectionString,
                                RelativeLocation = string.Format("{0}/input1datablob.csv", StorageContainerName)
                            }
                        },
                    },

                    Outputs = new Dictionary<string, AzureBlobDataReference>()
                    {

                        {
                            "output1",
                            new AzureBlobDataReference()
                            {
                                ConnectionString = storageConnectionString,
                                RelativeLocation = string.Format("/{0}/output1results.csv", StorageContainerName)
                            }
                        },
                    },
                    GlobalParameters = new Dictionary<string, string>() {
        { "File has header row", "" },
}
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", apiKey);
                client.Timeout = TimeSpan.FromMinutes(20);

                // WARNING: The 'await' statement below can result in a deadlock if you are calling this code from the UI thread of an ASP.Net application.
                // One way to address this would be to call ConfigureAwait(false) so that the execution does not attempt to resume on the original context.
                // For instance, replace code such as:
                //      result = await DoSomeTask()
                // with the following:
                //      result = await DoSomeTask().ConfigureAwait(false)


                //Console.WriteLine("Submitting the job...");

                // submit the job
                var response = await client.PostAsJsonAsync(BaseUrl + "?api-version=2.0", request);
                if (!response.IsSuccessStatusCode)
                {
                    //await WriteFailedResponse(response);
                    return;
                }

                string jobId = await response.Content.ReadAsAsync<string>();
                //Console.WriteLine(string.Format("Job ID: {0}", jobId));


                // start the job
                //Console.WriteLine("Starting the job...");
                response = await client.PostAsync(BaseUrl + "/" + jobId + "/start?api-version=2.0", null);
                if (!response.IsSuccessStatusCode)
                {
                    //await WriteFailedResponse(response);
                    return;
                }

                string jobLocation = BaseUrl + "/" + jobId + "?api-version=2.0";
                Stopwatch watch = Stopwatch.StartNew();
                bool done = false;

                response = await client.GetAsync(jobLocation);

                //try
                //{
                //    while (!done)
                //    {
                //        //Console.WriteLine("Checking the job status...");
                //        response = await client.GetAsync(jobLocation);
                //        if (!response.IsSuccessStatusCode)
                //        {
                //            //await WriteFailedResponse(response);
                //            return;
                //        }

                //        BatchScoreStatus status = await response.Content.ReadAsAsync<BatchScoreStatus>();
                //        if (watch.ElapsedMilliseconds > TimeOutInMilliseconds)
                //        {
                //            done = true;
                //            //Console.WriteLine(string.Format("Timed out. Deleting job {0} ...", jobId));
                //            await client.DeleteAsync(jobLocation);
                //        }
                //        switch (status.StatusCode)
                //        {
                //            case BatchScoreStatusCode.NotStarted:
                //                //Console.WriteLine(string.Format("Job {0} not yet started...", jobId));
                //                break;
                //            case BatchScoreStatusCode.Running:
                //                //Console.WriteLine(string.Format("Job {0} running...", jobId));
                //                break;
                //            case BatchScoreStatusCode.Failed:
                //                //Console.WriteLine(string.Format("Job {0} failed!", jobId));
                //                //Console.WriteLine(string.Format("Error details: {0}", status.Details));
                //                done = true;
                //                break;
                //            case BatchScoreStatusCode.Cancelled:
                //                //Console.WriteLine(string.Format("Job {0} cancelled!", jobId));
                //                done = true;
                //                break;
                //            case BatchScoreStatusCode.Finished:
                //                done = true;
                //                //Console.WriteLine(string.Format("Job {0} finished!", jobId));

                //                ProcessResults(status);
                //                break;
                //        }

                //        if (!done)
                //        {
                //            Thread.Sleep(1000); // Wait one second
                //        }
                //    }
                //}
                //catch (Exception ex)
                //{

                //}
            }
        }
    }
}