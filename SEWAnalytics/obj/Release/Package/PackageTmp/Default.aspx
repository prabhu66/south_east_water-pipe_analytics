﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SEWAnalytics.Default" %>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>South East Water</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- STYLESHEETS -->
	<!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/dashboard.css">

	<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- DATE RANGE PICKER -->

	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
</head>
<script>

    function validateForm() {
        var un = document.loginform.usr.value;
        var pw = document.loginform.pword.value;
        var username = "admin@gmail.com";
        var password = "admin";
        if ((un == username) && (pw == password)) {
            return true;
        }
        else {
            alert("Login was unsuccessful, please check your username and password");
            return false;
        }
    }
</script>
</script>

<body class="login body_bg">
	<!-- PAGE -->
	<section id="page">
		<!-- HEADER -->
		<header class="dashboard_header">
			<div class="col-md-12">
				<image src="images/SEWlogo.png" class="logo_dashboard"></image>
			</div>
		</header>
		<!--/HEADER -->
		<!-- LOGIN -->
		<section id="login" class="visible">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-md-offset-4">
						<div class="login-box-plain">
							<h2 class="bigintro">LOGIN</h2>

							<form role="form" runat="server">
								<div class="form-group">
									<label for="txtUserName1">ID</label>
									<i class="fa fa-envelope"></i>
									<input type="text" class="form-control" runat="server" id="txtUserName1" />
								</div>
								<div class="form-group">
									<label for="txtPWD1">Password</label>
									<i class="fa fa-lock"></i>
									<input type="password" runat="server" class="form-control" id="txtPWD1" />
								</div>
								<button type="submit" runat="server" id="btnSubmit1" onserverclick="btnSubmit_Click" class="btn btn_grn">Submit</button>

							</form>
							<!-- SOCIAL LOGIN -->
							<div class="divide-20"></div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/LOGIN -->

	</section>


</body>

</html>


