﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SEWAnalytics
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string login = txtUserName1.Value.Trim();
            string passwd = txtPWD1.Value;
            string passwdValue = ConfigurationManager.AppSettings[login];

            if (passwdValue != null)
            {

                if (passwdValue.Equals(passwd))
                {

                    Server.Transfer("Video.aspx");
                }
                else
                {
                    //lblError.Text = "Login Authentication Failed";
                }
            }
        }
    }
}